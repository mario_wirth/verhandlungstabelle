# Verhandlungstabelle für Verhandlungen!! 

### Wichtige Links ###

* [Download neueste Version](https://bitbucket.org/mario_wirth/verhandlungstabelle/raw/master/verhandlungstabelle.xls)
* [Download ursprüngliche Tabelle](https://bitbucket.org/mario_wirth/verhandlungstabelle/raw/version-1/verhandlungstabelle.xls)
* [Änderungen ](https://bitbucket.org/mario_wirth/verhandlungstabelle/commits/branch/master)
* [Aufgaben](https://bitbucket.org/mario_wirth/verhandlungstabelle/issues)
